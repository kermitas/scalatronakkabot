package main.scala.org.kermitas.scalatron.bot.akka
{
  import java.awt._

  object SwingDrawerConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config , readOutdatedCellOccupantFadeOutColor : Boolean ) : Option[SwingDrawerConfig] =
    {
      if( typesafeConfig.getBoolean( "enabled" ) )
      {
        val squareSizeInPixels = typesafeConfig.getInt( "squareSizeInPixels" )

        val outdatedCellOccupantFadeOutColorAsRGB =
        {
          if( readOutdatedCellOccupantFadeOutColor )
          {
            val intList = typesafeConfig.getIntList( "outdatedCellOccupantFadeOutColorAsRGB" )
            new Color( intList.get(0) , intList.get(1) , intList.get(2) )
          }
          else
          {
            Color.white
          }
        }

        Some( new SwingDrawerConfig( squareSizeInPixels , outdatedCellOccupantFadeOutColorAsRGB ) )
      }
      else
        None
    }
  }

  case class SwingDrawerConfig( squareSizeInPixels : Int , outdatedCellOccupantFadeOutColorAsRGB : Color )
}