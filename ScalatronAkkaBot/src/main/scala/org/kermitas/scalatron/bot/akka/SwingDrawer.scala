package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.util._
  import main.scala.org.kermitas.scalatron.opcodes._
  import javax.swing._
  import java.awt._

  object SwingDrawer
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object SemiInitialized extends State
      case object Initialized extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case class SemiInitializedStateData( swingDrawerConfig : SwingDrawerConfig , var windowTitle : String , squareSizeInPixels : Int , var createAsActive : Boolean , var statusText : String , swingWindowPlacer : ActorRef , outdatedCellOccupantWeightDecreaseStep : Int ) extends StateData
      case class InitializedStateData( mainWindowJFrame : JFrame , mapDrawerJPanel : MapDrawerJPanel , statusTextJLabel : JLabel ) extends StateData

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class Init( swingDrawerConfig : SwingDrawerConfig , windowTitle : String , statusText : String , swingWindowPlacer : ActorRef , outdatedCellOccupantWeightDecreaseStep : Int ) extends IncomingMessages
        case class DrawView( latestCellOccupantsArray : Array[ Array[ CellOccupy ] ] , weightsArrayOption : Option[ Array[ Array[ Byte ] ] ] ) extends IncomingMessages
        case class SetWindowTitle( windowTitle : String ) extends IncomingMessages
        case class SetStatusText( statusText : String ) extends IncomingMessages
        case class SetWindowActivity( isMapActive : Boolean ) extends IncomingMessages
        case class Stop( throwableOption : Option[ Throwable ] ) extends IncomingMessages

    // ----
  }

  class SwingDrawer extends Actor with FSM[SwingDrawer.State, SwingDrawer.StateData]
  {
    // ----

    startWith( SwingDrawer.Uninitialized , SwingDrawer.UninitializedStateData )

    when( SwingDrawer.Uninitialized )
    {
      case Event( SwingDrawer.Init( swingDrawerConfig , windowTitle , statusText , swingWindowPlacer , outdatedCellOccupantWeightDecreaseStep ) , SwingDrawer.UninitializedStateData ) ⇒
      {
        goto( SwingDrawer.SemiInitialized ) using new SwingDrawer.SemiInitializedStateData( swingDrawerConfig , windowTitle , swingDrawerConfig.squareSizeInPixels , true , statusText , swingWindowPlacer , outdatedCellOccupantWeightDecreaseStep )
      }
    }

    when( SwingDrawer.SemiInitialized )
    {
      case Event( SwingDrawer.SetStatusText( statusText ) , stateData : SwingDrawer.SemiInitializedStateData ) ⇒
      {
        stateData.statusText = statusText
        stay using stateData
      }

      case Event( SwingDrawer.SetWindowTitle( windowTitle ) , stateData : SwingDrawer.SemiInitializedStateData ) ⇒
      {
        stateData.windowTitle = windowTitle
        stay using stateData
      }

      case Event( SwingDrawer.SetWindowActivity( isMapActive ) , stateData : SwingDrawer.SemiInitializedStateData ) ⇒
      {
        stateData.createAsActive = false
        stay using stateData
      }

      case Event( SwingDrawer.DrawView( latestCellOccupantsArray , weightsArrayOption ) , stateData : SwingDrawer.SemiInitializedStateData ) ⇒
      {
        val jFrameJPanelTuple = createJFrameAndMapDrawerJPanelAndJLabel( latestCellOccupantsArray(0).size , latestCellOccupantsArray.size , stateData.squareSizeInPixels , stateData.windowTitle , stateData.createAsActive , stateData.statusText , stateData.swingWindowPlacer , stateData.swingDrawerConfig.outdatedCellOccupantFadeOutColorAsRGB , stateData.outdatedCellOccupantWeightDecreaseStep )

        val mainWindowJFrame : JFrame = jFrameJPanelTuple._1
        val swingDrawerJPanel : MapDrawerJPanel = jFrameJPanelTuple._2
        val statusTextJLabel : JLabel = jFrameJPanelTuple._3

        drawView( latestCellOccupantsArray , weightsArrayOption , swingDrawerJPanel )

        goto( SwingDrawer.Initialized ) using new SwingDrawer.InitializedStateData( mainWindowJFrame , swingDrawerJPanel , statusTextJLabel )
      }
    }

    when( SwingDrawer.Initialized )
    {
      case Event( SwingDrawer.SetStatusText( statusText ) , stateData : SwingDrawer.InitializedStateData ) ⇒
      {
        stateData.statusTextJLabel.setText( statusText )
        stay using stateData
      }

      case Event( SwingDrawer.SetWindowTitle( windowTitle ) , stateData : SwingDrawer.InitializedStateData ) ⇒
      {
        stateData.mainWindowJFrame.setTitle( windowTitle )

        stay using stateData
      }

      case Event( SwingDrawer.SetWindowActivity( isMapActive ) , stateData : SwingDrawer.InitializedStateData ) ⇒
      {
        decorateJPaneActivity( stateData.mapDrawerJPanel , isMapActive )

        stay using stateData
      }

      case Event( SwingDrawer.DrawView( latestCellOccupantsArray , weightsArrayOption ) , stateData : SwingDrawer.InitializedStateData ) ⇒
      {
        drawView( latestCellOccupantsArray , weightsArrayOption , stateData.mapDrawerJPanel )
        stay using stateData
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( SwingDrawer.Stop( throwableOption ) , stateData ) ⇒
      {
        throwableOption match
        {
          case Some( throwable ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )
          case None ⇒ stop( FSM.Normal )
        }
      }

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }

        stateData match
        {
          case initializedStateData : SwingDrawer.InitializedStateData =>
          {
            initializedStateData.mainWindowJFrame.setVisible( false )
            initializedStateData.mainWindowJFrame.remove( initializedStateData.mapDrawerJPanel )
            initializedStateData.mainWindowJFrame.remove( initializedStateData.statusTextJLabel )
            initializedStateData.mainWindowJFrame.dispose
          }

          case _ =>
        }
      }
    }

    initialize

    // ----

    protected def createJFrameAndMapDrawerJPanelAndJLabel( mapXInSquares : Int , mapYInSquares : Int , squareSizeInPixels : Int , windowTitle : String , createAsActive : Boolean , statusText : String , swingWindowPlacer : ActorRef , fadeOutColorAsRGB : Color , outdatedCellOccupantWeightDecreaseStep : Int ) : ( JFrame , MapDrawerJPanel , JLabel ) =
    {
      val mainWindowJFrame = new JFrame( windowTitle )

      mainWindowJFrame.setDefaultCloseOperation( WindowConstants.HIDE_ON_CLOSE )

      val mapDrawerJPanel = new MapDrawerJPanel( mapXInSquares , mapYInSquares , squareSizeInPixels , fadeOutColorAsRGB , outdatedCellOccupantWeightDecreaseStep )
      val statusTextJLabel = new JLabel( statusText )

      decorateJPaneActivity( mapDrawerJPanel , createAsActive )

      mainWindowJFrame.setLayout( new BorderLayout )

      mainWindowJFrame.add( mapDrawerJPanel , BorderLayout.CENTER )
      mainWindowJFrame.add( statusTextJLabel , BorderLayout.SOUTH )

      mainWindowJFrame.pack

      askPlacerForLocationCoordinates( swingWindowPlacer , mainWindowJFrame )

      mainWindowJFrame.setResizable( false )

      mainWindowJFrame.setVisible( true )

      ( mainWindowJFrame , mapDrawerJPanel , statusTextJLabel )
    }

    protected def askPlacerForLocationCoordinates( swingWindowPlacer : ActorRef , jFrame : JFrame )
    {
      val jFrameDimension = jFrame.getSize

      import scala.concurrent.duration._
      import _root_.akka.util.Timeout
      import _root_.akka.pattern.ask
      implicit val waitForResponseMessageTimeout = Timeout(5 seconds)

      val startStatusFuture = swingWindowPlacer ? new SwingWindowPlacer.GetWindowLocation( jFrameDimension.width , jFrameDimension.height )

      implicit val dispatcherForFuture = context.system.dispatcher

      startStatusFuture.onSuccess
      {
        case ( x : Int , y : Int ) ⇒ jFrame.setLocation( x , y )
      }
    }

    protected def decorateJPaneActivity( drawerJPanel : JPanel , active : Boolean )
    {
      drawerJPanel.setBorder( BorderFactory.createLineBorder( if(active) Color.green else Color.red , 2 ) )
    }

    protected def drawView( latestCellOccupantsArray : Array[ Array[ CellOccupy ] ] , weightsArrayOption : Option[ Array[ Array[ Byte ] ] ] , mapDrawerJPanel : MapDrawerJPanel )
    {
      mapDrawerJPanel.setCellOccupantsArray( latestCellOccupantsArray , weightsArrayOption )

      //jframe.invalidate
      //SwingUtilities.updateComponentTreeUI( jFrame )

      mapDrawerJPanel.invalidate
      SwingUtilities.updateComponentTreeUI( mapDrawerJPanel )
    }

    // ----
  }

  class MapDrawerJPanel( mapXInSquares : Int , mapYInSquares : Int , squareSizeInPixels : Int , outdatedCellColor : Color , outdatedCellOccupantWeightDecreaseStep : Int ) extends javax.swing.JPanel
  {
    // ---- colors

    protected val gridLinesColor = Color.darkGray

    protected val emptyColor = Color.lightGray
    protected val wallColor = Color.black
    protected val zugarColor = new Color( 0 , 150 , 0 )
    protected val toxiferaColor = new Color( 150 , 150 , 0 )
    protected val fluppetColor = new Color( 0 , 0 , 150 )
    protected val snorgColor = new Color( 150 , 0 , 0 )

    protected val myMasterOrMiniBotOuterInnerCircle = ( Color.yellow , Color.green )
    protected val enemyMasterOrMiniBotOuterInnerCircleColor = ( Color.red , Color.darkGray )

    protected val flashingColorForFadedOutCellOccupy = Color.red

    // ----

    protected var cellOccupiesArray : Array[ Array[ CellOccupy ] ] = null
    protected var weightsArrayOption : Option[ Array[ Array[ Byte ] ] ] = null

    // ---- precalculations

    protected val masterBotInnerRectSquareSizeDecreaserInPixels = (squareSizeInPixels * 0.3).toInt
    protected val doubleMinusOneMasterBotInnerRectSquareSizeDecreaserInPixels = masterBotInnerRectSquareSizeDecreaserInPixels*2 - 1

    protected val miniBotOuterOvalSquareSizeDecreaserInPixels = (squareSizeInPixels * 0.2).toInt
    protected val doubleMinusOneMiniBotOuterOvalSquareSizeDecreaserInPixels = miniBotOuterOvalSquareSizeDecreaserInPixels*2 - 1

    protected val miniBotInnerRectSquareSizeDecreaserInPixels = (squareSizeInPixels * 0.5).toInt
    protected val doubleMinusOneMiniBotInnerRectSquareSizeDecreaserInPixels = miniBotInnerRectSquareSizeDecreaserInPixels*2 - 1

    // ---- JPanel configuration

    {
      val insets = getInsets
      val d = new Dimension( mapXInSquares*squareSizeInPixels + insets.left + insets.right + 1 , mapYInSquares*squareSizeInPixels + insets.top + insets.bottom + 1 )
      setSize( d )
      setPreferredSize( d )
      setMaximumSize( d )
      setMinimumSize( d )

      this.setBackground( Color.white )
      setOpaque( true )
    }

    // ----

    def setCellOccupantsArray( cellOccupiesArray : Array[ Array[ CellOccupy ] ] , weightsArrayOption : Option[ Array[ Array[ Byte ] ] ] )
    {
      this.cellOccupiesArray = cellOccupiesArray
      this.weightsArrayOption = weightsArrayOption
    }

    override def paintComponent( g : Graphics )
    {
      super.paintComponent( g )

      drawGrid( g )
      drawCellOccupants( g )
    }

    protected def drawGrid( g : Graphics )
    {
      val maxXInPixels = mapXInSquares*squareSizeInPixels
      val maxYInPixels = mapYInSquares*squareSizeInPixels

      g.setColor( gridLinesColor )
      for( x <- Range.inclusive( 0 , maxXInPixels , squareSizeInPixels ) ) g.drawLine( x , 0 , x , maxYInPixels )
      for( y <- Range.inclusive( 0 , maxYInPixels , squareSizeInPixels ) ) g.drawLine( 0 , y , maxXInPixels , y )
    }

    protected def drawCellOccupants( g : Graphics )
    {
      for( y <- 0 until mapYInSquares; x <- 0 until mapXInSquares )
      {
        cellOccupiesArray(y)(x) match
        {
          case Unknown =>

          case Empty => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor(emptyColor,x,y) , g )

          case Wall => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , wallColor , g )

          case MyMasterBot => drawMasterBot( x * squareSizeInPixels , y * squareSizeInPixels , myMasterOrMiniBotOuterInnerCircle , g )

          case EnemyMasterBot => drawMasterBot( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor( enemyMasterOrMiniBotOuterInnerCircleColor , x , y ) , g )

          case MyMiniBot => drawMiniBot( x * squareSizeInPixels , y * squareSizeInPixels , myMasterOrMiniBotOuterInnerCircle , g )

          case EnemyMiniBot => drawMiniBot( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor( enemyMasterOrMiniBotOuterInnerCircleColor , x , y ) , g )

          case Zugar => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor(zugarColor,x,y) , g )

          case Toxifera => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor(toxiferaColor,x,y) , g )

          case Fluppet => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor(fluppetColor,x,y) , g )

          case Snorg => drawRect( x * squareSizeInPixels , y * squareSizeInPixels , fadeOutColor(snorgColor,x,y) , g )

          case unknown => throw new RuntimeException( "Unknown cell occupant '" + unknown + "' (at position " + x + ":" + y + ")" )
        }
      }
    }

    protected def fadeOutColor( twoColors : ( Color , Color) , x : Int , y : Int ) : ( Color , Color) =
    {
      if( weightsArrayOption.isEmpty )
      {
        twoColors
      }
      else
      {
        if( weightsArrayOption.get(y)(x) == 100 )
        {
          twoColors
        }
        else
        {
          val c1 = fadeOutColor( twoColors._1 , weightsArrayOption.get(y)(x) )
          val c2 = fadeOutColor( twoColors._2 , weightsArrayOption.get(y)(x) )

          ( c1 , c2 )
        }
      }
    }

    protected def fadeOutColor( color : Color , x : Int , y : Int ) : Color =
    {
      if( weightsArrayOption.isEmpty )
      {
        color
      }
      else
      {
        if( weightsArrayOption.get(y)(x) == 100 )
        {
          color
        }
        else
        {
          fadeOutColor( color , weightsArrayOption.get(y)(x) )
        }
      }
    }

    protected def fadeOutColor( color : Color , weight : Byte ) : Color =
    {
      if( weight < 20 && (weight/outdatedCellOccupantWeightDecreaseStep)%2==0 )
      {
        flashingColorForFadedOutCellOccupy
      }
      else
      {
        val p : Float = weight.toFloat / 100

        val red = color.getRed * p + outdatedCellColor.getRed * ( 1 - p)
        val green = color.getGreen * p + outdatedCellColor.getGreen * ( 1 - p)
        val blue = color.getBlue * p + outdatedCellColor.getBlue * ( 1 - p)

        new Color( red.toInt , green.toInt , blue.toInt )
      }
    }

    protected def drawMasterBot( cellLeft : Int , cellTop : Int , outerInnerCircleColor : ( Color , Color) , g : Graphics )
    {
      g.setColor( outerInnerCircleColor._1 )
      g.fillOval( cellLeft , cellTop , squareSizeInPixels , squareSizeInPixels )

      g.setColor( outerInnerCircleColor._2 )
      g.fillRect( cellLeft + masterBotInnerRectSquareSizeDecreaserInPixels , cellTop + masterBotInnerRectSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMasterBotInnerRectSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMasterBotInnerRectSquareSizeDecreaserInPixels )
    }

    protected def drawMiniBot( cellLeft : Int , cellTop : Int , outerInnerCircleColor : ( Color , Color ) , g : Graphics )
    {
      g.setColor( outerInnerCircleColor._1 )
      g.fillOval( cellLeft + miniBotOuterOvalSquareSizeDecreaserInPixels , cellTop + miniBotOuterOvalSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMiniBotOuterOvalSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMiniBotOuterOvalSquareSizeDecreaserInPixels )

      g.setColor( outerInnerCircleColor._2 )
      g.fillRect( cellLeft + miniBotInnerRectSquareSizeDecreaserInPixels , cellTop + miniBotInnerRectSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMiniBotInnerRectSquareSizeDecreaserInPixels , squareSizeInPixels - doubleMinusOneMiniBotInnerRectSquareSizeDecreaserInPixels )
    }

    protected def drawRect( cellLeft : Int , cellTop : Int , color : Color , g : Graphics )
    {
      g.setColor( color )
      g.fillRect( cellLeft + 1 , cellTop + 1 , squareSizeInPixels - 1 , squareSizeInPixels - 1 )
    }
  }
}