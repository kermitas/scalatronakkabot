package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import main.scala.org.kermitas.scalatron.opcodes._

  // -----

  object MasterMiniBotConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : MasterMiniBotConfig =
    {
      // TODO rename Configuration -> Config
      val masterBotConfiguration = MasterBotConfiguration( typesafeConfig.getConfig( "master" ) )

      // TODO rename Configuration -> Config
      val miniBotConfiguration = MiniBotConfiguration( typesafeConfig.getConfig( "mini" ) )

      new MasterMiniBotConfig( masterBotConfiguration=masterBotConfiguration , miniBotConfiguration=miniBotConfiguration )
    }
  }

  // TODO rename Configuration -> Config
  case class MasterMiniBotConfig( masterBotConfiguration : MasterBotConfiguration , miniBotConfiguration : MiniBotConfiguration )

  // -----

  // TODO rename Configuration -> Config
  object MasterBotConfiguration
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : MasterBotConfiguration =
    {
      val timeouts = MasterMiniBotTimeoutsConfig( typesafeConfig.getConfig( "timeouts" ) )

      val swingDrawerConfigOption = SwingDrawerConfig( typesafeConfig.getConfig( classOf[ SwingDrawer ].getSimpleName ) , false )

      new MasterBotConfiguration( timeouts , swingDrawerConfigOption )
    }
  }

  // TODO rename Configuration -> Config
  case class MasterBotConfiguration( timeouts : MasterMiniBotTimeoutsConfig , swingDrawerConfigOption : Option[ SwingDrawerConfig ] )

  // -----

  // TODO rename Configuration -> Config
  object MiniBotConfiguration
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : MiniBotConfiguration =
    {
      val timeouts = MasterMiniBotTimeoutsConfig( typesafeConfig.getConfig( "timeouts" ) )

      val leaveDeadMiniBotViewsOnScreenToTheEndOfTheGame = typesafeConfig.getBoolean( "leaveDeadMiniBotViewsOnScreenToTheEndOfTheGame" )

      val swingDrawerConfigOption = SwingDrawerConfig( typesafeConfig.getConfig( classOf[ SwingDrawer ].getSimpleName ) , false )

      new MiniBotConfiguration( timeouts , leaveDeadMiniBotViewsOnScreenToTheEndOfTheGame , swingDrawerConfigOption )
    }
  }

  // TODO rename Configuration -> Config
  case class MiniBotConfiguration( timeouts : MasterMiniBotTimeoutsConfig , leaveDeadMiniBotViewsOnScreenToTheEndOfTheGame : Boolean , swingDrawerConfigOption : Option[SwingDrawerConfig] )

  // -----

  object MasterMiniBotTimeoutsConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) : MasterMiniBotTimeoutsConfig =
    {
      val incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds = typesafeConfig.getInt( "incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds" )
      val responseFromMapTimeoutInSeconds = typesafeConfig.getInt( "responseFromMapTimeoutInSeconds" )
      val responseFromLogicTimeoutInSeconds = typesafeConfig.getInt( "responseFromLogicTimeoutInSeconds" )
      val whenMessageForwarderCheckIfContainsChildActorsIntervalInSeconds = typesafeConfig.getInt( "whenMessageForwarderCheckIfContainsChildActorsIntervalInSeconds" )

      new MasterMiniBotTimeoutsConfig( incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds , responseFromMapTimeoutInSeconds , responseFromLogicTimeoutInSeconds , whenMessageForwarderCheckIfContainsChildActorsIntervalInSeconds )
    }
  }

  case class MasterMiniBotTimeoutsConfig( incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds : Int , responseFromMapTimeoutInSeconds : Int , responseFromLogicTimeoutInSeconds : Int , whenMessageForwarderCheckIfContainsChildActorsIntervalInSeconds : Int )

  // -----

  case class MasterMiniBotRuntimeConfig( scalatronName : String , amIMasterBot : Boolean , maxStepsPerRound : Int , maxSlavesOption : Option[ Int ] , swingDrawerOption : Option[ActorRef] , swingWindowPlacer : ActorRef , gameMapKeeper : ActorRef , gameLogic : ActorRef , messageBroadcaster : ActorRef , var childrenNamingCounter : Int , var lastMoveOpcodeDirectionOption : Option[ Direction ] , var positionOnMap : java.awt.Point , var lastActivityTime : Int )

  // -----
}