package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.util._
  import main.scala.org.kermitas.scalatron.opcodes._
  import main.scala.org.kermitas.scalatron.bot.GameMap
  import java.awt.Point

  object GameLogic01
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object Initialized extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case class InitializedStateData( rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) extends StateData

    // ----

    sealed trait Messages
      sealed trait IncomingMessages extends Messages
        case class Init( messageBroadcaster : ActorRef ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

    // ----
  }

  class GameLogic01 extends Actor with FSM[GameLogic01.State, GameLogic01.StateData]
  {
    startWith( GameLogic01.Uninitialized , GameLogic01.UninitializedStateData )

    when( GameLogic01.Uninitialized )
    {
      case Event( GameLogic01.Init( messageBroadcaster ) , GameLogic01.UninitializedStateData ) ⇒
      {
        messageBroadcaster ! new MessageBroadcaster.Register( self , message => message.isInstanceOf[ MasterMiniBot.MiniBotDiedNotification ] )
        //messageBroadcaster ! new MessageBroadcaster.Register( self , message => message.isInstanceOf[ main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification ] )

        val rolesMap = new scala.collection.mutable.HashMap[ String , RoleRecord ]()

        goto( GameLogic01.Initialized ) using new GameLogic01.InitializedStateData( rolesMap )
      }
    }

    when( GameLogic01.Initialized )
    {
      case Event( doLogic : GameLogic.DoLogic , stateData : GameLogic01.InitializedStateData ) ⇒
      {
        val roleRecordOption : Option[ RoleRecord ] = stateData.rolesMap.get( doLogic.scalatronName ) match
        {
          case Some( roleRecord ) => Some( roleRecord )

          case None =>
          {
            if( doLogic.isMasterBot )
            {
              val roleRecord = new MasterBotRoleRecord01
              stateData.rolesMap.put( doLogic.scalatronName , roleRecord )
              Some( roleRecord )
            }
            else
              None
          }
        }

        roleRecordOption match
        {
          case Some( roleRecord ) ⇒
          {
            val outgoingOpcodesSeq : Seq[ OutgoingOpcode ] = roleRecord.doLogic( doLogic , stateData.rolesMap )

            sender ! new GameLogic.LogicResponse( outgoingOpcodesSeq )

            stay using stateData
          }

          case None ⇒ stop( FSM.Failure( new RuntimeException( "Could not find role for this '" + doLogic.scalatronName + "' mini bot." ) ) )
        }
      }

      case Event( miniBotDiedNotification : MasterMiniBot.MiniBotDiedNotification , stateData : GameLogic01.InitializedStateData ) ⇒
      {
        //log.debug( "MiniBot died!!!! " + miniBotDiedNotification.miniBotScalatronName )

        //log.debug( " ====================== " + miniBotDiedNotification.miniBotScalatronName )

        stateData.rolesMap.remove( miniBotDiedNotification.miniBotScalatronName )
        stay using stateData
      }

      //case Event( newTimeNotification : main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification , stateData : GameLogic01.InitializedStateData ) ⇒
      //{
        //log.debug( "########################### " + newTimeNotification )
      //  stay using stateData
      //}
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( GameLogic01.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }
      }
    }

    initialize

    // ----
  }

  // ==========

  trait RoleRecord
  {
    def doLogic( doLogic : GameLogic.DoLogic , rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) : Seq[ OutgoingOpcode ]
  }

  // ----

  class MasterBotRoleRecord01 extends RoleRecord
  {
    protected val maxBreadwinners = 1

    protected val spawnedBreadwinnerEnergy = 100
    protected val breadwinnerEnergyTarget = 250

    def doLogic( doLogic : GameLogic.DoLogic , rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) : Seq[ OutgoingOpcode ] =
    {
      var outgoingOpcodesSeq : Seq[ OutgoingOpcode ] = List[ OutgoingOpcode ]()



      val myBreadwinnersCount = howMuchBreadwinnerDoIHave( rolesMap )

      if( doLogic.abstractBotReact.energy > spawnedBreadwinnerEnergy && myBreadwinnersCount < maxBreadwinners )
      {

        getDirectionToSomethingToEatNearMe( doLogic.positionOnMap , doLogic.gameMap , 20 ) match
        {
          case Some( directionToSomethingToEatNearMe ) =>
          {
            val breadWinnerMiniBotRole = new BreadWinnerMiniBotRole( directionToSomethingToEatNearMe , breadwinnerEnergyTarget )

            val newMiniBotName = doLogic.childrenNamingCounter.toString

            rolesMap.put( newMiniBotName , breadWinnerMiniBotRole )

            val spawnOpcode = new Spawn( directionToSomethingToEatNearMe , newMiniBotName , spawnedBreadwinnerEnergy , None )
            outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
          }

          case None =>
          {
            /*
            val directionCreator: (Int) => Direction = ( tryNumber ) =>
            {
              //val z = myBreadwinnersCount%4 + createNumber

              val direction : Direction = tryNumber % 8 match
              {
                case 0 => new Direction( Left , Up )
                case 1 => new Direction( Stay , Up )
                case 2 => new Direction( Right , Up )
                case 3 => new Direction( Right , Stay )
                case 4 => new Direction( Right , Down )
                case 5 => new Direction( Stay , Down )
                case 6 => new Direction( Left , Down )
                case 7 => new Direction( Left , Stay )
              }

              direction
            }
            */

            GameLogicUtils.randomObstaclesAvoidingDirection( doLogic.positionOnMap , doLogic.gameMap , false , 12 , true , true , false ) match
            {
              case Some( breadWinnerMiniBotDirection ) =>
              {
                val breadWinnerMiniBotRole = new BreadWinnerMiniBotRole( breadWinnerMiniBotDirection , breadwinnerEnergyTarget )

                val newMiniBotName = doLogic.childrenNamingCounter.toString

                rolesMap.put( newMiniBotName , breadWinnerMiniBotRole )

                val spawnOpcode = new Spawn( breadWinnerMiniBotDirection , newMiniBotName , spawnedBreadwinnerEnergy , None )
                outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
              }

              case None =>
            }
          }
        }



        /*
        MoveUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true ) match
        {
          case Some( correctRandomBornDirection ) =>
          {
            val breadWinnerMinibotRole = new BreadWinnerMinibotRole

            val newMiniBotName = doLogic.childrenNamingCounter.toString

            rolesMap.put( newMiniBotName , breadWinnerMinibotRole )

            val spawnOpcode = new Spawn( correctRandomBornDirection , newMiniBotName , 100 , None )
            outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
          }

          case None =>
        }*/
      }

      /*
      MoveUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , true , 10 , true , true ) match
      {
        case Some( randomCorrectDirection ) => outgoingOpcodesSeq = new Move( randomCorrectDirection , None ) +: outgoingOpcodesSeq
        case None =>
      }

      if( _root_.scala.util.Random.nextInt( 8 ) == 4 && doLogic.abstractBotReact.energy > 150 )
      {
        MoveUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true ) match
        {
          case Some( correctRandomBornDirection ) =>
          {
            val spawnOpcode = new Spawn( correctRandomBornDirection , doLogic.childrenNamingCounter.toString , doLogic.abstractBotReact.energy - 50 , None )
            outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
          }

          case None =>
        }
      }
      */

      outgoingOpcodesSeq
    }

    protected def getDirectionToSomethingToEatNearMe( positionOnMap : Point , gameMap : GameMap , maxDistance : Int ) : Option[ Direction ] =
    {
      val cellOccupyChecker : (CellOccupy) => Boolean = _.isInstanceOf[ CanBeEatenByMyMiniBot ]

      GameLogicUtils.findNearMe( positionOnMap , gameMap , maxDistance , cellOccupyChecker ) match
      {
        case Some( pointWithSomethingToEatNearMe ) => Some( GameLogicUtils.calculateDirection( positionOnMap , pointWithSomethingToEatNearMe ) )
        case None => None
      }

    }

    protected def howMuchBreadwinnerDoIHave( rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) : Int = rolesMap.count( kv => kv._2.isInstanceOf[ BreadWinnerMiniBotRole ] )
  }

  // ----

  class BreadWinnerMiniBotRole( generalDirection : Direction , backToMasterBotAfterCollectingEnergy : Int ) extends RoleRecord
  {
    def doLogic( doLogic : GameLogic.DoLogic , rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) : Seq[ OutgoingOpcode ] =
    {
      var outgoingOpcodesSeq : Seq[ OutgoingOpcode ] = List[ OutgoingOpcode ]()

      if( doLogic.abstractBotReact.energy > backToMasterBotAfterCollectingEnergy )
      {
        val miniBotReact = doLogic.abstractBotReact.asInstanceOf[ MiniBotReact ]

        GameLogicUtils.suggestedObstaclesAvoidingDirection( doLogic.positionOnMap , doLogic.gameMap , false , true , true , true , GameLogicUtils.miniBotMasterParameterToDirectionToMaster( miniBotReact.master ) ) match
        {
          case Some( moveDirection ) => outgoingOpcodesSeq = new Move( moveDirection , None ) +: outgoingOpcodesSeq
          case None =>
        }
      }
      else
      {
        // uwaga: moj master nie jest ani hard ani soft obstacle a jednak nie chcemy na niego wejsc!!

        getDirectionToSomethingToEatNearMe( doLogic.positionOnMap , doLogic.gameMap , 10 ) match
        {
          case Some( directionToSomethingToEatNearMe ) => outgoingOpcodesSeq = new Move( directionToSomethingToEatNearMe , None ) +: outgoingOpcodesSeq
          case None =>
        }

        if( outgoingOpcodesSeq.size == 0 )
        {
          GameLogicUtils.suggestedObstaclesAvoidingDirection( doLogic.positionOnMap , doLogic.gameMap , false , true , true , true , generalDirection ) match
          {
            case Some( moveDirection ) => outgoingOpcodesSeq = new Move( moveDirection , None ) +: outgoingOpcodesSeq
            case None =>
          }
        }
      }
      /*
      var i = 0
      val directionCreator: () => Direction = () =>
      {
        val direction : Direction = i match
        {
          case 0 => generalDirection
          case 1 => new Direction( MoveUtils.randomDirection.horizontalDirection , generalDirection.verticalDirection )
          case 2 => new Direction( generalDirection.horizontalDirection , MoveUtils.randomDirection.verticalDirection )
          case 3 => MoveUtils.randomDirection
        }

        i += 1
        i = i%4

        direction
      }

      MoveUtils.obstaclesAvoidingDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true , directionCreator ) match
      {
        case Some( obstaclesAvoidingDirection ) => outgoingOpcodesSeq = new Move( obstaclesAvoidingDirection , None ) +: outgoingOpcodesSeq
        case None =>
      }*/

      // isc w wyznaczonym kierunku (jesli sie da); jesli wall to zmienic troche kierunek ale nie cofac sie do mastera
      // jesli w odleglosci X jedzenie to natychmiast namiar na jedzenie i iscie na niego (z omijaniem przeszkod)
      // po zjedzeniu czegokolwiek jadalnego powrot na mastera


      /*if( _root_.scala.util.Random.nextInt( 8 ) == 4 && doLogic.abstractBotReact.energy > 150 )
      {
        MoveUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true ) match
        {
          case Some( correctRandomBornDirection ) =>
          {
            val spawnOpcode = new Spawn( correctRandomBornDirection , doLogic.scalatronName + "-" + doLogic.childrenNamingCounter , doLogic.abstractBotReact.energy - 50 , None )
            outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
          }

          case None =>
        }
      }*/

      outgoingOpcodesSeq
    }

    protected def getDirectionToSomethingToEatNearMe( positionOnMap : Point , gameMap : GameMap , maxDistance : Int ) : Option[ Direction ] =
    {
      val cellOccupyChecker : (CellOccupy) => Boolean = _.isInstanceOf[ CanBeEatenByMyMiniBot ]

      GameLogicUtils.findNearMe( positionOnMap , gameMap , maxDistance , cellOccupyChecker ) match
      {
        case Some( pointWithSomethingToEatNearMe ) => Some( GameLogicUtils.calculateDirection( positionOnMap , pointWithSomethingToEatNearMe ) )
        case None => None
      }

    }
      /*
      def checkSquare( leftUpX : Int , leftTopY : Int , side : Int ) : Option[ Direction ] =
      {
        for( xi <- 0 until side )
        {


          gameMap( gameMap.toArrayY( leftTopY ) )( gameMap.toArrayX( leftUpX + xi ) ) match
          {
            case s : CanBeEatenByMyMiniBot => return Some( MoveUtils.calculateDirection( leftUpX , leftTopY , leftUpX + xi , leftTopY ) )
            case _ =>
          }
        }

        None
      }

      for( i <- 1 to distance )
      {
        val directionOption = checkSquare( positionOnMap.x - i , positionOnMap.y - i , i + 2 )
        if( !directionOption.isEmpty )  return directionOption
      }

      None
      */
    //}

    /*
    protected def moveInGeneralDirection( positionOnMap : Point , gameMap : GameMap ) : Option[ Direction ] =
    {
      val directionCreator: (Int) => Direction = ( createNumber ) =>
      {
        val direction : Direction = createNumber%4 match
        {
          case 0 => generalDirection
          case 1 => new Direction( GameLogicUtils.randomDirection.horizontalDirection , generalDirection.verticalDirection )
          case 2 => new Direction( generalDirection.horizontalDirection , GameLogicUtils.randomDirection.verticalDirection )
          case 3 => GameLogicUtils.randomDirection
        }

        direction
      }

      GameLogicUtils.obstaclesAvoidingDirection( positionOnMap , gameMap , false , 10 , true , true , directionCreator )
    }*/

  }

  // ----
  /*
  class MiniBotRoleRecord extends RoleRecord
  {
    def doLogic( doLogic : GameLogic.DoLogic , rolesMap : scala.collection.mutable.HashMap[ String , RoleRecord ] ) : Seq[ OutgoingOpcode ] =
    {
      var outgoingOpcodesSeq : Seq[ OutgoingOpcode ] = List[ OutgoingOpcode ]()

      GameLogicUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true ) match
      {
        case Some( direction ) => outgoingOpcodesSeq = new Move( direction , None ) +: outgoingOpcodesSeq
        case None =>
      }

      if( _root_.scala.util.Random.nextInt( 8 ) == 4 && doLogic.abstractBotReact.energy > 150 )
      {
        GameLogicUtils.randomCorrectDirection( doLogic.positionOnMap , doLogic.gameMap , false , 10 , true , true ) match
        {
          case Some( correctRandomBornDirection ) =>
          {
            val spawnOpcode = new Spawn( correctRandomBornDirection , doLogic.scalatronName + "-" + doLogic.childrenNamingCounter , doLogic.abstractBotReact.energy - 50 , None )
            outgoingOpcodesSeq = spawnOpcode +: outgoingOpcodesSeq
          }

          case None =>
        }
      }

      outgoingOpcodesSeq
    }
  }
  */

  // ==========
}