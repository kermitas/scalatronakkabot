package main.scala.org.kermitas.scalatron.bot.akka
{
  // Very drity code and dirty actor - just a proof of concept of swing windows on screen placer

  import akka.actor._
  import akka.event._
  import akka.util._
  import javax.swing._
  import java.awt._

  object SwingWindowPlacer
  {
    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class GetWindowLocation( windowWidth : Int , windowHeight : Int ) extends IncomingMessages
  }

  class SwingWindowPlacer extends Actor
  {
    protected val log = Logging(context.system, this)

    protected val marginX = 100 //670

    protected val screenSize =
    {
      val ss = Toolkit.getDefaultToolkit.getScreenSize
      new Dimension( ss.width-marginX , ss.height )
    }

    protected var maxX = screenSize.width
    protected var maxY = 0
    protected var maxWindowHeight = 0

    def receive =
    {
      case SwingWindowPlacer.GetWindowLocation( windowWidth , windowHeight ) ⇒
      {
        try
        {
          if( windowWidth > maxX )
          {
            maxX = screenSize.width
            maxY = maxY+maxWindowHeight+25
            maxWindowHeight = 0
          }

          val x = maxX - windowWidth
          val y = maxY

          maxX = x
          if( windowHeight > maxWindowHeight ) maxWindowHeight = windowHeight

          sender ! ( x+marginX , y )
        }
        catch
        {
          case throwable : Throwable => sender ! ( 0 , 0 )
        }
      }

      case m ⇒ log.warning( "Received unknown message " + m )
    }
  }
}