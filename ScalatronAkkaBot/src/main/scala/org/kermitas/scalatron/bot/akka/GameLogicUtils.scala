package main.scala.org.kermitas.scalatron.bot.akka
{
  import main.scala.org.kermitas.scalatron.opcodes._
  import main.scala.org.kermitas.scalatron.bot.GameMap
  import java.awt.Point

  object GameLogicUtils
  {
    // ----

    def randomDirection( considerStayDirection : Boolean ) : Direction =
    {
      val randomMaxInt = if( considerStayDirection ) 3 else 2

      val horizontalDirection : HorizontalDirections = _root_.scala.util.Random.nextInt( randomMaxInt ) match
      {
        case 0 ⇒ Left
        case 1 ⇒ Right
        case 2 ⇒ Stay
      }

      val verticalDirection : VerticalDirections = _root_.scala.util.Random.nextInt( randomMaxInt ) match
      {
        case 0 ⇒ Up
        case 1 ⇒ Down
        case 2 ⇒ Stay
      }

      new Direction( horizontalDirection , verticalDirection )
    }

    // ----

    def isHardObstacleAtPosition( gameMap : GameMap , positionToCheck : Point , amIMasterBoot : Boolean ) : Boolean =
    {
      if( amIMasterBoot )
      {
        gameMap.getCellOccupyUsingCenterBased( positionToCheck ).isInstanceOf[ HardObstacleForMyMasterBot ]

        /*
        gameMap.getCellOccupyUsingCenterBased( positionToCheck ) match
        {
          case _ : HardObstacleForMyMasterBot => true
          case _ => false
        }*/
      }
      else
      {
        gameMap.getCellOccupyUsingCenterBased( positionToCheck ).isInstanceOf[ HardObstacleForMyMiniBot ]

        /*
        gameMap.getCellOccupyUsingCenterBased( positionToCheck ) match
        {
          case _ : HardObstacleForMyMiniBot => true
          case _ => false
        }
        */
      }

      /*
      gameMap.getCellOccupyUsingCenterBased( positionToCheck ) match
      {
        case _ : HardObstacleForMyMasterBot if amIMasterBoot => true
        case _ : HardObstacleForMyMiniBot if !amIMasterBoot => true
        case _ => false
      }*/
    }

    // ----

    def isSoftObstacleAtPosition( gameMap : GameMap , positionToCheck : Point , amIMasterBoot : Boolean ) : Boolean =
    {
      gameMap.getCellOccupyUsingCenterBased( positionToCheck ) match
      {
        case _ : WillHurtMyMasterBot if amIMasterBoot => true
        case _ : WillHurtMyMiniBot if !amIMasterBoot => true
        case _ => false
      }
    }

    // ----

    def obstaclesAvoidingDirection( positionOnMap : Point , gameMap : GameMap , amIMasterBoot : Boolean , retriesCount : Int , avoidHardObstacles : Boolean , avoidSoftObstacles : Boolean , directionCreator : (Int) => Direction ) : Option[ Direction ] =
    {
      for( tryNumber <- 0 until retriesCount )
      {
        val direction = directionCreator( tryNumber )

        val positionToCheck = new Point( positionOnMap.x + direction.horizontalDirection.getDirectionAsInt , positionOnMap.y + direction.verticalDirection.getDirectionAsInt )

        val hardObstacleCheck = avoidHardObstacles && isHardObstacleAtPosition( gameMap , positionToCheck , amIMasterBoot )
        val softObstacleCheck = avoidSoftObstacles && isSoftObstacleAtPosition( gameMap , positionToCheck , amIMasterBoot )

        if( !hardObstacleCheck && !softObstacleCheck ) return Some( direction )
      }

      None
    }

    // ----

    def randomObstaclesAvoidingDirection( positionOnMap : Point , gameMap : GameMap , amIMasterBoot : Boolean , retriesCount : Int , avoidHardObstacles : Boolean , avoidSoftObstacles : Boolean , considerStayDirection : Boolean ) : Option[ Direction ] =
    {
      obstaclesAvoidingDirection( positionOnMap , gameMap , amIMasterBoot , retriesCount , avoidHardObstacles , avoidSoftObstacles , _ => randomDirection( considerStayDirection ) )
    }

    // ----

    def suggestedObstaclesAvoidingDirection( positionOnMap : Point , gameMap : GameMap , amIMasterBoot : Boolean , avoidHardObstacles : Boolean , avoidSoftObstacles : Boolean , considerStayDirection : Boolean , suggestedDirection : Direction ) : Option[ Direction ] =
    {
      def getSuggestedDirectionNumber : Int =
      {
        suggestedDirection match
        {
          case Direction( Stay , Up ) => 0
          case Direction( Right , Up ) => 1
          case Direction( Right , Stay ) => 2
          case Direction( Right , Down ) => 3
          case Direction( Stay , Down ) => 4
          case Direction( Left , Down ) => 5
          case Direction( Left , Stay ) => 6
          case Direction( Left , Up ) => 7
          case Direction( Stay , Stay ) => 8
        }
      }

      def getDirectionFromNumber( number : Int ) : Direction =
      {
        number match
        {
          case 0 => new Direction( Stay , Up )
          case 1 => new Direction( Right , Up )
          case 2 => new Direction( Right , Stay )
          case 3 => new Direction( Right , Down )
          case 4 => new Direction( Stay , Down )
          case 5 => new Direction( Left , Down )
          case 6 => new Direction( Left , Stay )
          case 7 => new Direction( Left , Up )
          case 8 => new Direction( Stay , Stay )

        }
      }

      def normalizeDirectionNumber( number : Int , exclusiveMax : Int ) : Int =
      {
        if( number >= 0 && number < exclusiveMax ) number
        else
        if( number < 0 ) exclusiveMax + number
        else
          number - exclusiveMax
      }

      val dirNr = getSuggestedDirectionNumber

      val directionCreator: (Int) => Direction = ( tryNumber ) =>
      {
        val direction : Direction = tryNumber match
        {
          case 0 => suggestedDirection

          case 1 => getDirectionFromNumber( normalizeDirectionNumber( dirNr - 1 , 8 ) )
          case 2 => getDirectionFromNumber( normalizeDirectionNumber( dirNr + 1 , 8 ) )

          case 3 => getDirectionFromNumber( normalizeDirectionNumber( dirNr - 2 , 8 ) )
          case 4 => getDirectionFromNumber( normalizeDirectionNumber( dirNr + 2 , 8 ) )

          case 5 => getDirectionFromNumber( normalizeDirectionNumber( dirNr - 3 , 8 ) )
          case 6 => getDirectionFromNumber( normalizeDirectionNumber( dirNr + 3 , 8 ) )

          case 7 => getDirectionFromNumber( normalizeDirectionNumber( dirNr - 4 , 8 ) )

          case 8 => getDirectionFromNumber( 8 )

          //case 3 => randomDirection( considerStayDirection )
        }

        /*
        val direction : Direction = tryNumber % 4 match
        {
          case 0 => suggestedDirection
          case 1 => new Direction( randomDirection( considerStayDirection ).horizontalDirection , suggestedDirection.verticalDirection )
          case 2 => new Direction( suggestedDirection.horizontalDirection , randomDirection( considerStayDirection ).verticalDirection )
          case 3 => randomDirection( considerStayDirection )
        }
        */

        direction
      }

      val directionOptionsCountAsRetriesCount = 8 + ( if( considerStayDirection ) 1 else 0 )

      obstaclesAvoidingDirection( positionOnMap , gameMap , amIMasterBoot , directionOptionsCountAsRetriesCount , avoidHardObstacles , avoidSoftObstacles , directionCreator )
    }

    // ----

    def calculateDirection( basePoint : Point , toPoint : Point ) : Direction =
    {
      val horizontalDirection : HorizontalDirections = if( toPoint.x < basePoint.x ) Left else if( toPoint.x == basePoint.x ) Stay else Right
      val verticalDirection : VerticalDirections = if( toPoint.y < basePoint.y ) Up else if( toPoint.y == basePoint.y ) Stay else Down

      new Direction( horizontalDirection , verticalDirection )
    }

    // ----

    def findNearMe( positionOnMap : Point , gameMap : GameMap , maxDistance : Int , comparator : (CellOccupy) => Boolean ) : Option[ Point ] =
    {
      def checkSquare( leftTopX : Int , leftTopY : Int , squareSide : Int ) : Option[ Point ] =
      {
        def useComparator( x : Int , y : Int ) : Option[ Point ] = if( comparator( gameMap.getCellOccupyUsingCenterBased( x , y ) ) ) Some( new Point( x , y ) ) else None

        var pointOption : Option[ Point ] = null

        val squareSideMinusOne = squareSide - 1

        for( i <- 0 until squareSideMinusOne )
        {
          pointOption = useComparator( leftTopX + i , leftTopY )
          if( !pointOption.isEmpty ) return pointOption

          pointOption = useComparator( leftTopX + squareSideMinusOne , leftTopY + i )
          if( !pointOption.isEmpty ) return pointOption

          pointOption = useComparator( leftTopX + squareSideMinusOne - i , leftTopY + squareSideMinusOne )
          if( !pointOption.isEmpty ) return pointOption

          pointOption = useComparator( leftTopX , leftTopY + squareSideMinusOne - i )
          if( !pointOption.isEmpty ) return pointOption
        }

        None
      }

      for( i <- 1 to maxDistance )
      {
        val pointOption = checkSquare( positionOnMap.x - i , positionOnMap.y - i , i*2 + 1 )

        if( !pointOption.isEmpty ) return pointOption
      }

      None
    }

    // ----

    def miniBotMasterParameterToDirectionToMaster( master : Point ) : Direction =
    {
      val horizontalDirection : HorizontalDirections =
      {
        if( master.x < 0 ) Left
        else
        if( master.x == 0 ) Stay
        else
          Right
      }

      val verticalDirection : VerticalDirections =
      {
        if( master.y < 0 ) Up
        else
        if( master.y == 0 ) Stay
        else
          Down
      }

      new Direction( horizontalDirection , verticalDirection )
    }

    // ----
  }
}