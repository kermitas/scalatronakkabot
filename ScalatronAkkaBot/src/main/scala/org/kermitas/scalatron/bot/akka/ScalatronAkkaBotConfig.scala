package main.scala.org.kermitas.scalatron.bot.akka
{
  object ScalatronAkkaBotConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) =
    {
      val logLevel = java.util.logging.Level.parse( typesafeConfig.getString( "logLevel" ) )
      val workAsAkkaRemoteDeploymentNode = typesafeConfig.getBoolean( "workAsAkkaRemoteDeploymentNode" )

      val akkaNormalModeTypesafeConfig = typesafeConfig.getConfig( "AkkaNormalMode" )

      val checkIfUserPressedAnyKeyIntervalInMilliSeconds = akkaNormalModeTypesafeConfig.getInt("checkIfUserPressedAnyKeyIntervalInMilliSeconds")

      val mainActorExecuteInitialMessageTimeoutInSeconds = akkaNormalModeTypesafeConfig.getInt("mainActorExecuteInitialMessageTimeoutInSeconds")

      val botConfig = main.scala.org.kermitas.scalatron.bot.BotConfig( akkaNormalModeTypesafeConfig.getConfig( classOf[ main.scala.org.kermitas.scalatron.bot.Bot ].getSimpleName ) )

      new ScalatronAkkaBotConfig( logLevel , workAsAkkaRemoteDeploymentNode , checkIfUserPressedAnyKeyIntervalInMilliSeconds , mainActorExecuteInitialMessageTimeoutInSeconds , botConfig )
    }
  }

  case class ScalatronAkkaBotConfig( logLevel : java.util.logging.Level , workAsAkkaRemoteDeploymentNode : Boolean , checkIfUserPressedAnyKeyIntervalInMilliSeconds : Int , mainActorExecuteInitialMessageTimeoutInSeconds : Int , botConfig : _root_.main.scala.org.kermitas.scalatron.bot.BotConfig )
}