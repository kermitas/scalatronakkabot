package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.util._
  import main.scala.org.kermitas.scalatron.opcodes._

  object GameMapKeeper
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object Initialized extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case class InitializedStateData( gameMapKeeperConfig : GameMapKeeperConfig , var gameMap : main.scala.org.kermitas.scalatron.bot.GameMap , swingDrawerOption : Option[ActorRef] , messageBroadcaster : ActorRef ) extends StateData

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class Init( gameMapKeeperConfig : GameMapKeeperConfig , windowTitle : String , swingWindowPlacer : ActorRef , messageBroadcaster : ActorRef ) extends IncomingMessages
        case class UpdateMap( lastPositionOnMap : java.awt.Point , lastMoveOpcodeDirectionOption : Option[Direction] , wasCollision : Boolean , view : View ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class UpdatedMap( gameMap : main.scala.org.kermitas.scalatron.bot.GameMap , newPositionOnMap : java.awt.Point ) extends OutgoingMessages

    // ----

  }

  class GameMapKeeper extends Actor with FSM[GameMapKeeper.State, GameMapKeeper.StateData]
  {
    startWith( GameMapKeeper.Uninitialized , GameMapKeeper.UninitializedStateData )

    when( GameMapKeeper.Uninitialized )
    {
      case Event( GameMapKeeper.Init( gameMapKeeperConfig , windowTitle , swingWindowPlacer , messageBroadcaster ) , GameMapKeeper.UninitializedStateData ) ⇒
      {
        val swingDrawerOption  : Option[ ActorRef ] = gameMapKeeperConfig.swingDrawerConfigOption match
        {
          case Some( swingDrawerConfig ) ⇒
          {
            val swingDrawer = context.actorOf( Props[ SwingDrawer ] , classOf[ SwingDrawer ].getSimpleName )

            swingDrawer ! new SwingDrawer.Init( swingDrawerConfig , "General map for '" + windowTitle  + "' bot game." , "time=??" , swingWindowPlacer , gameMapKeeperConfig.outdatedCellOccupantWeightDecreaseStep )
            Some( swingDrawer )
          }

          case None ⇒ None
        }

        messageBroadcaster ! new MessageBroadcaster.Register( self , message => message.isInstanceOf[ main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification ] )

        val gameMap = main.scala.org.kermitas.scalatron.bot.GameMap( gameMapKeeperConfig.xInSquares , gameMapKeeperConfig.yInSquares )

        goto( GameMapKeeper.Initialized ) using new GameMapKeeper.InitializedStateData( gameMapKeeperConfig , gameMap , swingDrawerOption , messageBroadcaster )
      }
    }

    when( GameMapKeeper.Initialized )
    {
      case Event( GameMapKeeper.UpdateMap( lastPositionOnMap , lastMoveOpcodeDirectionOption , wasCollision , view ) , stateData : GameMapKeeper.InitializedStateData ) ⇒
      {
        val newPositionOnMap = lastMoveOpcodeDirectionOption match
        {
          case Some( lastMoveOpcodeDirection ) =>
          {
            if( !wasCollision ) // last move confirmed
            {
              new java.awt.Point( lastPositionOnMap.x + lastMoveOpcodeDirection.horizontalDirection.getDirectionAsInt , lastPositionOnMap.y + lastMoveOpcodeDirection.verticalDirection.getDirectionAsInt )
            }
            else // last move did not happen - collision
            {
              lastPositionOnMap
            }
          }

          case None => lastPositionOnMap
        }

        doMapUpdate( newPositionOnMap , view , stateData )

        sender ! new GameMapKeeper.UpdatedMap( stateData.gameMap , newPositionOnMap )

        stay using stateData
      }

      case Event( miniBotDiedNotification : MasterMiniBot.MiniBotDiedNotification , stateData : GameMapKeeper.InitializedStateData ) ⇒
      {
        doMapUpdate( miniBotDiedNotification , stateData )
        stay using stateData
      }

      case Event( newTimeNotification : main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification , stateData : GameMapKeeper.InitializedStateData ) ⇒
      {
        stateData.swingDrawerOption.foreach{ _ ! new SwingDrawer.SetStatusText( "time=" + newTimeNotification.newTime ) }

        for( y <- 0 until stateData.gameMap.mapHeight ; x <- 0 until stateData.gameMap.mapWidth )
        {
          stateData.gameMap.cellOccupiesArray(y)(x) match
          {
            case Unknown =>
            case Empty =>
            case MyMasterBot =>
            case MyMiniBot =>
            case Wall =>
            case _ =>
            {
              if( stateData.gameMap.weightsArray(y)(x) > 0 )
              {
                stateData.gameMap.weightsArray(y)(x) = ( stateData.gameMap.weightsArray(y)(x) - stateData.gameMapKeeperConfig.outdatedCellOccupantWeightDecreaseStep ).toByte

                if( stateData.gameMap.weightsArray(y)(x) <= 0 )
                {
                  stateData.gameMap.cellOccupiesArray(y)(x) = Empty
                  stateData.gameMap.weightsArray(y)(x) = 100
                }
              }
            }
          }
        }

        stateData.swingDrawerOption.foreach{ _ ! new SwingDrawer.DrawView( stateData.gameMap.cellOccupiesArray , Some( stateData.gameMap.weightsArray ) ) }
        stay using stateData
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( GameMapKeeper.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }

        stateData match
        {
          case initializedStateData : GameMapKeeper.InitializedStateData => initializedStateData.swingDrawerOption.foreach{ _ ! new SwingDrawer.Stop( None ) }
          case _ =>
        }
      }
    }

    initialize

    // ----

    protected def doMapUpdate( positionOnMap : java.awt.Point , view : View , stateData : GameMapKeeper.InitializedStateData )
    {
      stateData.gameMap = main.scala.org.kermitas.scalatron.bot.GameMap.update( positionOnMap , view , stateData.gameMap )
      stateData.swingDrawerOption.foreach{ _ ! new SwingDrawer.DrawView( stateData.gameMap.cellOccupiesArray , Some( stateData.gameMap.weightsArray ) ) }
    }

    protected def doMapUpdate( miniBotDiedNotification : MasterMiniBot.MiniBotDiedNotification , stateData : GameMapKeeper.InitializedStateData )
    {
      // TODO somehow remove dead mini-bot from map and then request for redraw
      //stateData.swingDrawerOption.foreach{ _ ! new SwingDrawer.DrawView( stateData.gameMap.array ) }
    }

    // ----
  }
}