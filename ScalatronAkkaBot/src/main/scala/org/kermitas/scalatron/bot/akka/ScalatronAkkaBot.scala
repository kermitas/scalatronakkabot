package main.scala.org.kermitas.scalatron.bot.akka
{
  import akka.actor._
  import akka.event._
  import akka.io._
  import scala.concurrent.duration._

  object ScalatronAkkaBot
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object Working extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
      case object WorkingStateData extends StateData

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages

        case class Start( scalatronAkkaBotConfig : ScalatronAkkaBotConfig ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages

        case class StartStatus( throwableOption : Option[Throwable] ) extends OutgoingMessages

    // ----
  }

  class ScalatronAkkaBot extends Actor with FSM[ScalatronAkkaBot.State, ScalatronAkkaBot.StateData]
  {
    startWith( ScalatronAkkaBot.Uninitialized , ScalatronAkkaBot.UninitializedStateData )

    when( ScalatronAkkaBot.Uninitialized )
    {
      case Event( ScalatronAkkaBot.Start( scalatronAkkaBotConfig ) , ScalatronAkkaBot.UninitializedStateData ) ⇒
      {
        try
        {
          startKeyListener( scalatronAkkaBotConfig.checkIfUserPressedAnyKeyIntervalInMilliSeconds )

          main.scala.org.kermitas.scalatron.bot.Bot.setBotConfig( scalatronAkkaBotConfig.botConfig )

          sender ! ScalatronAkkaBot.StartStatus( None )

          goto( ScalatronAkkaBot.Working ) using ScalatronAkkaBot.WorkingStateData
        }
        catch
        {
          case t : Throwable ⇒
          {
            sender ! ScalatronAkkaBot.StartStatus( Some( t ) )
            stop( FSM.Failure( t ) )
          }
        }
      }
    }

    when( ScalatronAkkaBot.Working )
    {
      // just a dummy event because this state is completely empty
      case Event( StateTimeout , stateData ) ⇒ stay using stateData
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( ScalatronAkkaBot.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )
      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stoping (normal), state " + state + ", data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stoping (shutdown), state " + state + ", data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stoping (failure = " + cause + "), state " + state + ", data " + stateData )
        }

        new _root_.main.scala.org.kermitas.scalatron.util.JVMKillingThread( 4 , false ).start

        context.system.shutdown
      }
    }

    initialize

    // ----

    protected def startKeyListener( checkIfUserPressedAnyKeyIntervalInMilliSeconds : Int )
    {
      import main.scala.org.kermitas.scalatron.util._
      val keyPressedListenerActor = context.actorOf( Props[ KeyPressedListenerActor ] , name = classOf[ KeyPressedListenerActor ].getSimpleName )
      keyPressedListenerActor ! new KeyPressedListenerActor.Init( self , new ScalatronAkkaBot.Stop( new Throwable( "User requested stop" ) ) , checkIfUserPressedAnyKeyIntervalInMilliSeconds )
      log.info( "Press enter to shutdown" )
    }
  }
}
