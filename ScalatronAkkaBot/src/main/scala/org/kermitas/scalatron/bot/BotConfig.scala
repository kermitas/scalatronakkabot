package main.scala.org.kermitas.scalatron.bot
{
  import akka._

  object BotConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) =
    {
      val incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds = typesafeConfig.getInt("incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds")
      val masterOrMiniBotResponseTimeoutInSeconds = typesafeConfig.getInt("masterOrMiniBotResponseTimeoutInSeconds")

      val gameMapKeeperConfig : GameMapKeeperConfig = GameMapKeeperConfig( typesafeConfig.getConfig( classOf[ GameMapKeeper ].getSimpleName ) )

      val masterMiniBotConfig : MasterMiniBotConfig = MasterMiniBotConfig( typesafeConfig.getConfig( classOf[ MasterMiniBot ].getSimpleName  ) )

      new BotConfig( incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds , masterOrMiniBotResponseTimeoutInSeconds , gameMapKeeperConfig , masterMiniBotConfig )
    }
  }

  case class BotConfig( incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds : Int , masterOrMiniBotResponseTimeoutInSeconds : Int , gameMapKeeperConfig : GameMapKeeperConfig , masterMiniBotConfig : MasterMiniBotConfig )
}
