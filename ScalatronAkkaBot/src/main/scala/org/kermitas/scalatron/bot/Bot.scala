package main.scala.org.kermitas.scalatron.bot
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.io._
  import _root_.akka.util._
  import scala.concurrent.duration._
  import collection.immutable.List
  import main.scala.org.kermitas.scalatron.opcodes._

  object Bot
  {
    // ----

    sealed trait State
      case object WaitingForWelcomeOpcode extends State
      case object WaitingForIncomingOpcode extends State
      case object WaitingForMasterOrMiniBotResponse extends State

    // ----

    sealed trait StateData
      case object WaitingForWelcomeOpcodeStateData extends StateData
      class InitializedStateData( val welcomeOpcode : Welcome , val masterMiniBot : ActorRef , val gameMapKeeper : ActorRef , val swingWindowPlacer : ActorRef , val gameLogic : ActorRef , val eventBroadcaster : ActorRef ) extends StateData
        class WaitingForIncomingOpcodeStateData( welcomeOpcode : Welcome , masterMiniBot : ActorRef , val currentTime : Int , override val gameMapKeeper : ActorRef , override val swingWindowPlacer : ActorRef , override val gameLogic : ActorRef , override val eventBroadcaster : ActorRef ) extends InitializedStateData( welcomeOpcode , masterMiniBot , gameMapKeeper , swingWindowPlacer , gameLogic , eventBroadcaster )
        class WaitingForMasterOrMiniBotResponseStateData( welcomeOpcode : Welcome , masterMiniBot : ActorRef , val responseOpcodesSeqListener : ActorRef , val currentTime : Int , override val gameMapKeeper : ActorRef , override val swingWindowPlacer : ActorRef , override val gameLogic : ActorRef , override val eventBroadcaster : ActorRef ) extends InitializedStateData( welcomeOpcode , masterMiniBot , gameMapKeeper , swingWindowPlacer , gameLogic , eventBroadcaster )

    // ----

    sealed trait Messages
      sealed trait IncomingMessages extends Messages
        case class Stop( throwable : Throwable ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class NewTimeNotification( oldTime : Int , newTime : Int ) extends OutgoingMessages

    // ----

    protected var botConfig : BotConfig = null

    def setBotConfig( botConfig : BotConfig ){ this.botConfig = botConfig }

    // ----
  }

  class Bot extends Actor with FSM[Bot.State, Bot.StateData]
  {
    // TODO Configure: if any child actor will blow that should blow current actor too!!

    // ----

    startWith( Bot.WaitingForWelcomeOpcode , Bot.WaitingForWelcomeOpcodeStateData )

    when( Bot.WaitingForWelcomeOpcode , stateTimeout = Bot.botConfig.incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds second )
    {
      case Event( StateTimeout , Bot.WaitingForWelcomeOpcodeStateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while waiting for Welcome opcode" ) ) )
      }

      case Event( welcomeOpcode : Welcome , Bot.WaitingForWelcomeOpcodeStateData ) ⇒
      {
        log.debug( "Welcome opcode '" + welcomeOpcode + "', parent = " + context.parent + ", sender = " + sender )

        val messageBroadcaster = context.actorOf( Props[ akka.MessageBroadcaster ] , classOf[ akka.MessageBroadcaster ].getSimpleName )

        val swingWindowPlacer = context.actorOf( Props[ akka.SwingWindowPlacer ] , classOf[ akka.SwingWindowPlacer ].getSimpleName )

        val gameMapKeeper = context.actorOf( Props[ akka.GameMapKeeper ] , classOf[ akka.GameMapKeeper ].getSimpleName )
        gameMapKeeper ! new akka.GameMapKeeper.Init( Bot.botConfig.gameMapKeeperConfig , welcomeOpcode.name , swingWindowPlacer , messageBroadcaster )

        val gameLogic = context.actorOf( Props[ akka.GameLogic01 ] , classOf[ akka.GameLogic01 ].getSimpleName )
        gameLogic ! akka.GameLogic01.Init( messageBroadcaster )

        val masterMiniBot = context.actorOf( Props[ _root_.main.scala.org.kermitas.scalatron.bot.akka.MasterMiniBot ] , welcomeOpcode.name )
        masterMiniBot ! new _root_.main.scala.org.kermitas.scalatron.bot.akka.MasterMiniBot.Init( welcomeOpcode.name , true , welcomeOpcode.apocalypse , welcomeOpcode.maxSlavesOption , Bot.botConfig.masterMiniBotConfig , swingWindowPlacer , gameMapKeeper , gameLogic , new java.awt.Point(0,0) , 0 , messageBroadcaster )
        masterMiniBot ! welcomeOpcode

        messageBroadcaster ! welcomeOpcode

        goto( Bot.WaitingForMasterOrMiniBotResponse ) using new Bot.WaitingForMasterOrMiniBotResponseStateData( welcomeOpcode , masterMiniBot , sender , 0 , gameMapKeeper , swingWindowPlacer , gameLogic , messageBroadcaster )
      }
    }

    when( Bot.WaitingForIncomingOpcode , stateTimeout = Bot.botConfig.incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds second )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
        stop( FSM.Failure( new RuntimeException( "Timeout while waiting for incoming opcode" ) ) )
      }

      case Event( abstractBotReact : AbstractBotReact , stateData : Bot.WaitingForIncomingOpcodeStateData ) ⇒
      {
        if ( stateData.currentTime != abstractBotReact.time )
        {
          log.debug( "Change of game time (iteration) from " + stateData.currentTime + " to " + abstractBotReact.time )

          stateData.eventBroadcaster ! new Bot.NewTimeNotification( stateData.currentTime , abstractBotReact.time )
        }

        abstractBotReact match
        {
          case masterBotReactOpcode : MasterBotReact ⇒ stateData.masterMiniBot ! masterBotReactOpcode
          case miniBotReactOpcode : MiniBotReact ⇒ getMiniBotActor( stateData.welcomeOpcode.name , miniBotReactOpcode.name ) ! miniBotReactOpcode
        }

        goto( Bot.WaitingForMasterOrMiniBotResponse ) using new Bot.WaitingForMasterOrMiniBotResponseStateData( stateData.welcomeOpcode , stateData.masterMiniBot , sender , abstractBotReact.time , stateData.gameMapKeeper , stateData.swingWindowPlacer , stateData.gameLogic , stateData.eventBroadcaster )
      }
    }

    when( Bot.WaitingForMasterOrMiniBotResponse , stateTimeout = Bot.botConfig.masterOrMiniBotResponseTimeoutInSeconds second )
    {
      case Event( StateTimeout , stateData ) ⇒
      {
          stop( FSM.Failure( new RuntimeException( "Timeout while waiting for master or mini bot prepare outgoing opcode" ) ) )
      }

      case Event( outgoingOpcodesSeq : Seq[_] , stateData : Bot.WaitingForMasterOrMiniBotResponseStateData ) ⇒
      {
        // TODO it is good place to add something to this seq
        // TODO maybe analyze outgoing seq to monitor if it has duplicates of opcodes and log it

        stateData.responseOpcodesSeqListener ! outgoingOpcodesSeq

        goto( Bot.WaitingForIncomingOpcode ) using new Bot.WaitingForIncomingOpcodeStateData( stateData.welcomeOpcode , stateData.masterMiniBot , stateData.currentTime , stateData.gameMapKeeper , stateData.swingWindowPlacer , stateData.gameLogic , stateData.eventBroadcaster )
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( Bot.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )

      case Event( miniBotDiedNotification : akka.MasterMiniBot.MiniBotDiedNotification , stateData ) ⇒ stay using stateData

      case Event( goodbyeOpcode : Goodbye , stateData ) ⇒
      {
        log.debug( "Received Goodbye message '" + goodbyeOpcode + "'" )
        if( stateData.isInstanceOf[ Bot.InitializedStateData ] ) stateData.asInstanceOf[ Bot.InitializedStateData ].eventBroadcaster ! goodbyeOpcode
        stop( FSM.Normal )
      }

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent(stopType, state, stateData) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state '" + state + "', state data " + stateData )
        }
      }
    }

    initialize

    // ----

    protected def getMiniBotActor( masterBotName : String , miniBotName : String ) : ActorRef =
    {
      val path = masterBotName + "/" + miniBotName.replace( "-" , "/" )
      val miniBotActor = context.actorFor( path )

      //log.debug( "Mini bot actor under path '" + path + "' = " + miniBotActor )

      miniBotActor
    }
  }
}
