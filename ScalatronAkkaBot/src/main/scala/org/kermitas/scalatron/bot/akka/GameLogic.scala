package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import main.scala.org.kermitas.scalatron.opcodes._

  object GameLogic
  {
    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class DoLogic( abstractBotReact : AbstractBotReact , scalatronName : String , isMasterBot : Boolean , childrenNamingCounter : Int , positionOnMap : java.awt.Point , gameMap : main.scala.org.kermitas.scalatron.bot.GameMap ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class LogicResponse( response : Seq[ OutgoingOpcode ] ) extends OutgoingMessages

    // ----
  }
}