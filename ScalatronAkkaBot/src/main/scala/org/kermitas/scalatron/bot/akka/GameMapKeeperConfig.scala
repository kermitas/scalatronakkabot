package main.scala.org.kermitas.scalatron.bot.akka
{
  object GameMapKeeperConfig
  {
    def apply( typesafeConfig : com.typesafe.config.Config ) =
    {
      val xInSquares = typesafeConfig.getInt( "XInSquares" )
      val yInSquares = typesafeConfig.getInt( "YInSquares" )
      val outdatedCellOccupantWeightDecreaseStep = typesafeConfig.getInt( "outdatedCellOccupantWeightDecreaseStep" )

      val swingDrawerConfigOption = SwingDrawerConfig( typesafeConfig.getConfig( classOf[ SwingDrawer ].getSimpleName ) , true )

      new GameMapKeeperConfig( xInSquares , yInSquares , outdatedCellOccupantWeightDecreaseStep , swingDrawerConfigOption )
    }
  }

  case class GameMapKeeperConfig( xInSquares : Int , yInSquares : Int , outdatedCellOccupantWeightDecreaseStep : Int , swingDrawerConfigOption : Option[ SwingDrawerConfig ] )
}