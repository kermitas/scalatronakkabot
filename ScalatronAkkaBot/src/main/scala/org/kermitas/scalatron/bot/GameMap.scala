package main.scala.org.kermitas.scalatron.bot
{
  import main.scala.org.kermitas.scalatron.opcodes._
  import java.awt.Point

  object GameMap
  {
    def apply( mapX : Int , mapY : Int ) : GameMap =
    {
      val cellOccupiesArray : Array[ Array[ CellOccupy ] ] =
      {
        val cellOccupiesArray = new Array[ Array[ CellOccupy ] ]( mapY )

        for( y <- 0 until mapY )
        {
          val row = new Array[ CellOccupy ]( mapX )
          cellOccupiesArray( y ) = row

          for( x <- 0 until mapX ) row( x ) = Unknown
        }

        cellOccupiesArray
      }

      val weightsArray : Array[ Array[ Byte ] ] =
      {
        val weightsArray = new Array[ Array[ Byte ] ]( mapY )

        for( y <- 0 until mapY )
        {
          val row = new Array[ Byte ]( mapX )
          weightsArray( y ) = row

          for( x <- 0 until mapX ) row( x ) = 0
        }

        weightsArray
      }

      new GameMap( mapX , mapY , cellOccupiesArray , weightsArray )
    }

    def update( positionOnMap : java.awt.Point , view : main.scala.org.kermitas.scalatron.opcodes.View , oldGameMap : GameMap) : GameMap =
    {
      val s = (view.squareSide-1)/2

      val startXOnNewArray = ( ( oldGameMap.mapWidth / 2 ) + positionOnMap.x ) - s
      val startYOnNewArray = ( ( oldGameMap.mapHeight / 2 ) + positionOnMap.y ) - s

      for( y <- 0 until view.squareSide ; x <- 0 until view.squareSide )
      {
        if( view.viewArray(y)(x) != main.scala.org.kermitas.scalatron.opcodes.Unknown )
        {
          oldGameMap.cellOccupiesArray(startYOnNewArray+y)(startXOnNewArray+x) = view.viewArray(y)(x)

          oldGameMap.weightsArray(startYOnNewArray+y)(startXOnNewArray+x) = 100
        }
      }

      oldGameMap
    }
  }

  case class GameMap( mapWidth : Int , mapHeight : Int , cellOccupiesArray : Array[ Array[ CellOccupy ] ] , weightsArray : Array[ Array[ Byte ] ] )
  {
    protected def centerBasedXToArrayX( xCenterBased : Int ) : Int =
    {
      var xArrayBased = mapWidth / 2 + xCenterBased
      while( xArrayBased < 0 ) xArrayBased = mapWidth + xArrayBased
      while( xArrayBased >= mapWidth ) xArrayBased = xArrayBased - mapWidth
      xArrayBased
    }

    protected def centerBasedYToArrayY( yCenterBased : Int ) : Int =
    {
      var yArrayBased = mapHeight / 2 + yCenterBased
      while( yArrayBased < 0 ) yArrayBased = mapHeight + yArrayBased
      while( yArrayBased >= mapHeight ) yArrayBased = yArrayBased - mapHeight
      yArrayBased
    }

    def getCellOccupyUsingCenterBased( xCenterBased : Int , yCenterBased : Int ) : CellOccupy = cellOccupiesArray( centerBasedYToArrayY( yCenterBased ) )( centerBasedXToArrayX( xCenterBased ) )
    def getCellOccupyUsingCenterBased( centerBasedPoint : Point ) : CellOccupy = getCellOccupyUsingCenterBased( centerBasedPoint.x , centerBasedPoint.y )
  }
}