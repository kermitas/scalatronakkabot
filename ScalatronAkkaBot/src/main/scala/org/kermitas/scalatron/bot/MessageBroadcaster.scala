package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.util._
  import scala.concurrent.duration._

  object MessageBroadcaster
  {
    trait Messages
      trait IncomingMessages extends Messages
        case class Register( listener : ActorRef , classifier : (Any)=>Boolean ) extends IncomingMessages
        case class UnregisterClassifier( listener : ActorRef , classifier : (Any)=>Boolean ) extends IncomingMessages
        case class Unregister( listener : ActorRef ) extends IncomingMessages
        case object RemoveTerminatedActors extends IncomingMessages
  }

  class MessageBroadcaster extends Actor
  {
    protected val terminatedActorsCheckIntervalInSeconds = 20
    protected val map = new scala.collection.mutable.HashMap[ ActorRef , scala.collection.mutable.ListBuffer[ (Any)=>Boolean ] ]

    context.system.scheduler.schedule( terminatedActorsCheckIntervalInSeconds second , terminatedActorsCheckIntervalInSeconds second , self , MessageBroadcaster.RemoveTerminatedActors )( context.system.dispatcher )

    def receive =
    {
      // ----

      case MessageBroadcaster.Register( listener , classifier ) ⇒
      {
        map.get( listener ) match
        {
          case Some( classifiers ) ⇒
          {
            classifiers += classifier
          }

          case None ⇒
          {
            val classifiers = new scala.collection.mutable.ListBuffer[ (Any)=>Boolean ]
            map.put( listener , classifiers )
            classifiers += classifier
          }
        }
      }

      // ----

      case MessageBroadcaster.UnregisterClassifier( listener , classifier ) ⇒
      {
        map.get( listener ) match
        {
          case Some( classifiers ) ⇒
          {
            classifiers -= classifier
            if( classifiers.isEmpty ) map.remove( listener )
          }

          case None ⇒
        }
      }

      // ----

      case MessageBroadcaster.Unregister( listener ) ⇒
      {
        map.remove( listener )
      }

      // ----

      case MessageBroadcaster.RemoveTerminatedActors ⇒
      {
        val keysToRemove = new scala.collection.mutable.ListBuffer[ ActorRef ]
        map.keys.foreach( listener => if( listener.isTerminated ) keysToRemove += listener )
        keysToRemove.foreach( map.remove )

      }

      // ----

      case messageToBroadcast : AnyRef ⇒
      {
        val keysToRemove = new scala.collection.mutable.ListBuffer[ ActorRef ]

        map.iterator.foreach{ listenerWithClassifiers =>

          if( listenerWithClassifiers._1.isTerminated )
          {
            keysToRemove += listenerWithClassifiers._1
          }
          else
          {
            listenerWithClassifiers._2.find{ classifier =>
              try
              {
                if( classifier( messageToBroadcast ) )
                {
                  listenerWithClassifiers._1.tell( messageToBroadcast , sender )
                  true
                }
                else
                  false
              }
              catch{ case t : Throwable ⇒ false }
            }
          }

        }

        keysToRemove.foreach( map.remove )
      }

      // ----
    }
  }
}