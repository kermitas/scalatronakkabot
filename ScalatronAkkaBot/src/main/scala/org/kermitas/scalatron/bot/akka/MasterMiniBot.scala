package main.scala.org.kermitas.scalatron.bot.akka
{
  import _root_.akka.actor._
  import _root_.akka.event._
  import _root_.akka.io._
  import _root_.akka.util._
  import scala.concurrent.duration._
  import main.scala.org.kermitas.scalatron.opcodes._
  import java.awt.Point

  object MasterMiniBot
  {
    // ----

    sealed trait State
      case object Uninitialized extends State
      case object WaitingForIncomingOpcode extends State
      case object WaitingForMapUpdate extends State
      case object WaitingForLogicResponse extends State
      case object MessageForwarder extends State

    // ----

    sealed trait StateData
      case object UninitializedStateData extends StateData
        class InitializedStateData( val masterMiniBotConfig : MasterMiniBotConfig , val masterMiniBotRuntimeConfig : MasterMiniBotRuntimeConfig ) extends StateData
          case class WaitingForIncomingOpcodeStateData( override val masterMiniBotConfig : MasterMiniBotConfig , override val masterMiniBotRuntimeConfig : MasterMiniBotRuntimeConfig ) extends InitializedStateData( masterMiniBotConfig , masterMiniBotRuntimeConfig )
          case class WaitingForMapUpdateStateData( override val masterMiniBotConfig : MasterMiniBotConfig , override val masterMiniBotRuntimeConfig : MasterMiniBotRuntimeConfig , outgoingOpcodesSeqListener : ActorRef , abstractBotReact : AbstractBotReact ) extends InitializedStateData( masterMiniBotConfig , masterMiniBotRuntimeConfig )
          case class WaitingForLogicResponseStateData( override val masterMiniBotConfig : MasterMiniBotConfig , override val masterMiniBotRuntimeConfig : MasterMiniBotRuntimeConfig , outgoingOpcodesSeqListener : ActorRef ) extends InitializedStateData( masterMiniBotConfig , masterMiniBotRuntimeConfig )
          case class MessageForwarderStateData( override val masterMiniBotConfig : MasterMiniBotConfig , override val masterMiniBotRuntimeConfig : MasterMiniBotRuntimeConfig ) extends InitializedStateData( masterMiniBotConfig , masterMiniBotRuntimeConfig )

    // ----

    sealed trait Messages

      sealed trait IncomingMessages extends Messages
        case class Init( scalatronName : String , workAsMasterBot : Boolean , maxStepsPerRound : Int , maxSlavesOption : Option[ Int ] , masterMiniBotConfig : MasterMiniBotConfig , swingWindowPlacer : ActorRef , gameMapKeeper : ActorRef , gameLogic : ActorRef , startPositionOnMap : Point , time : Int , messageBroadcaster : ActorRef ) extends IncomingMessages
        case class Stop( throwable : Throwable ) extends IncomingMessages

      sealed trait OutgoingMessages extends Messages
        case class MiniBotDiedNotification( miniBotScalatronName : String ) extends OutgoingMessages

    // ----
  }

  class MasterMiniBot extends Actor with FSM[MasterMiniBot.State, MasterMiniBot.StateData]
  {
    // ----

    startWith( MasterMiniBot.Uninitialized , MasterMiniBot.UninitializedStateData )

    when( MasterMiniBot.Uninitialized )
    {
      case Event( MasterMiniBot.Init( scalatronName , workAsMasterBot , maxStepsPerRound , maxSlavesOption , masterMiniBotConfig , swingWindowPlacer , gameMapKeeper , gameLogic , startPositionOnMap , time , messageBroadcaster ) , MasterMiniBot.UninitializedStateData ) ⇒
      {
        val swingDrawerConfigOption : Option[ SwingDrawerConfig ] = if( workAsMasterBot ) masterMiniBotConfig.masterBotConfiguration.swingDrawerConfigOption else masterMiniBotConfig.miniBotConfiguration.swingDrawerConfigOption
        val swingDrawerOption : Option[ ActorRef ] = swingDrawerConfigOption match
        {
          case Some( swingDrawerConfig ) ⇒
          {
            val swingDrawer = context.actorOf( Props[ SwingDrawer ] , classOf[ SwingDrawer ].getSimpleName )

            val windowTitle = if( workAsMasterBot ) scalatronName + " (master bot)"  else scalatronName + " (mini bot)"

            swingDrawer ! new SwingDrawer.Init( swingDrawerConfig , windowTitle , "energy=??" , swingWindowPlacer , 0 )

            Some( swingDrawer )
          }

          case None ⇒ None
        }

        {
          val timeouts : MasterMiniBotTimeoutsConfig = if( workAsMasterBot ) masterMiniBotConfig.masterBotConfiguration.timeouts else masterMiniBotConfig.miniBotConfiguration.timeouts

          setStateTimeout( MasterMiniBot.WaitingForIncomingOpcode , Some( timeouts.incomingOpcodeFromScalatronAkkaServerTimeoutInSeconds second ) )
          setStateTimeout( MasterMiniBot.WaitingForMapUpdate , Some( timeouts.responseFromMapTimeoutInSeconds second ) )
          setStateTimeout( MasterMiniBot.WaitingForLogicResponse , Some( timeouts.responseFromLogicTimeoutInSeconds second ) )
          setStateTimeout( MasterMiniBot.MessageForwarder , Some( timeouts.whenMessageForwarderCheckIfContainsChildActorsIntervalInSeconds second ) )
        }

        messageBroadcaster ! new MessageBroadcaster.Register( self , message => message.isInstanceOf[ main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification ] )

        val masterMiniBotRuntimeConfig = new MasterMiniBotRuntimeConfig( scalatronName=scalatronName , amIMasterBot=workAsMasterBot , maxStepsPerRound=maxStepsPerRound , maxSlavesOption=maxSlavesOption , swingDrawerOption=swingDrawerOption , swingWindowPlacer=swingWindowPlacer , gameMapKeeper=gameMapKeeper , gameLogic=gameLogic , messageBroadcaster=messageBroadcaster , childrenNamingCounter=0 , lastMoveOpcodeDirectionOption=None , positionOnMap=startPositionOnMap , lastActivityTime=time )

        goto( MasterMiniBot.WaitingForIncomingOpcode ) using new MasterMiniBot.WaitingForIncomingOpcodeStateData( masterMiniBotConfig , masterMiniBotRuntimeConfig )
      }
    }

    when( MasterMiniBot.WaitingForIncomingOpcode )
    {
      case Event( StateTimeout , stateData : MasterMiniBot.WaitingForIncomingOpcodeStateData ) ⇒
      {
        gotoMessageForwardSateOrStop( scala.Left( "Timeout while waiting for incoming (from Scalatron via ScalatronAkkaServer) opcode" ) , stateData )
      }

      case Event( welcomeOpcode : Welcome , stateData : MasterMiniBot.WaitingForIncomingOpcodeStateData ) ⇒
      {
        val outgoingOpcodesSeq : Seq[ OutgoingOpcode ] = List[ OutgoingOpcode ]()
        sender ! outgoingOpcodesSeq

        stay using stateData
      }

      case Event( abstractBotReact : AbstractBotReact , stateData : MasterMiniBot.WaitingForIncomingOpcodeStateData ) ⇒
      {
        stateData.masterMiniBotRuntimeConfig.swingDrawerOption.foreach{ _ ! new SwingDrawer.DrawView( abstractBotReact.view.viewArray , None ) }

        stateData.masterMiniBotRuntimeConfig.lastActivityTime = abstractBotReact.time

        stateData.masterMiniBotRuntimeConfig.gameMapKeeper ! new GameMapKeeper.UpdateMap( stateData.masterMiniBotRuntimeConfig.positionOnMap , stateData.masterMiniBotRuntimeConfig.lastMoveOpcodeDirectionOption , !abstractBotReact.collisionOption.isEmpty , abstractBotReact.view )

        stateData.masterMiniBotRuntimeConfig.lastMoveOpcodeDirectionOption = None

        goto( MasterMiniBot.WaitingForMapUpdate ) using new MasterMiniBot.WaitingForMapUpdateStateData( stateData.masterMiniBotConfig , stateData.masterMiniBotRuntimeConfig , sender , abstractBotReact )
      }

      case Event( newTimeNotification : main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification , stateData : MasterMiniBot.WaitingForIncomingOpcodeStateData ) ⇒
      {
        //sendToAllMyChildrenMiniBots( newTimeNotification , stateData.masterMiniBotRuntimeConfig.swingDrawerOption )

        if( stateData.masterMiniBotRuntimeConfig.amIMasterBot )
        {
          stay using stateData
        }
        else
        {
          if ( newTimeNotification.newTime - stateData.masterMiniBotRuntimeConfig.lastActivityTime > 1 )
            gotoMessageForwardSateOrStop( scala.Left(  "Mini actor died (scalatron does not send new data for this mini actor)" ) , stateData )
          else
          {
            stay using stateData
          }
        }
      }
    }

    when( MasterMiniBot.WaitingForMapUpdate )
    {
      case Event( StateTimeout , stateData : MasterMiniBot.WaitingForMapUpdateStateData ) ⇒
      {
        gotoMessageForwardSateOrStop( scala.Left( "Timeout while waiting for map update" ) , stateData )
      }

      case Event( GameMapKeeper.UpdatedMap( gameMap , newPositionOnMap ) , stateData : MasterMiniBot.WaitingForMapUpdateStateData ) ⇒
      {
        stateData.masterMiniBotRuntimeConfig.positionOnMap = newPositionOnMap

        stateData.masterMiniBotRuntimeConfig.swingDrawerOption.foreach{ swingDrawer =>
          val collisionTxt = if( stateData.abstractBotReact.collisionOption.isEmpty ) " [OK] " else " [COLL]"
          swingDrawer ! new SwingDrawer.SetStatusText( "energy=" + stateData.abstractBotReact.energy + " pos=" + newPositionOnMap.x + ":" + newPositionOnMap.y + collisionTxt )
        }

        stateData.masterMiniBotRuntimeConfig.gameLogic ! new GameLogic.DoLogic( stateData.abstractBotReact , stateData.masterMiniBotRuntimeConfig.scalatronName , stateData.masterMiniBotRuntimeConfig.amIMasterBot , stateData.masterMiniBotRuntimeConfig.childrenNamingCounter , stateData.masterMiniBotRuntimeConfig.positionOnMap , gameMap )

        goto( MasterMiniBot.WaitingForLogicResponse ) using new MasterMiniBot.WaitingForLogicResponseStateData( stateData.masterMiniBotConfig , stateData.masterMiniBotRuntimeConfig , stateData.outgoingOpcodesSeqListener )
      }
    }

    when( MasterMiniBot.WaitingForLogicResponse )
    {
      case Event( StateTimeout , stateData : MasterMiniBot.WaitingForLogicResponseStateData ) ⇒
      {
        gotoMessageForwardSateOrStop( scala.Left(  "Timeout while waiting for game logic response" ) , stateData )
      }

      case Event( GameLogic.LogicResponse( outgoingOpcodesSeq ) , stateData : MasterMiniBot.WaitingForLogicResponseStateData ) ⇒
      {
        for( outgoingOpcode <- outgoingOpcodesSeq )
        {
          outgoingOpcode match
          {
            case moveOpcode : Move ⇒ stateData.masterMiniBotRuntimeConfig.lastMoveOpcodeDirectionOption = Some( moveOpcode.direction )
            case spawnOpcode : Spawn ⇒ spawnMiniBotActor( stateData , spawnOpcode )
            case _ ⇒
          }
        }

        stateData.outgoingOpcodesSeqListener ! outgoingOpcodesSeq

        goto( MasterMiniBot.WaitingForIncomingOpcode ) using new MasterMiniBot.WaitingForIncomingOpcodeStateData( stateData.masterMiniBotConfig , stateData.masterMiniBotRuntimeConfig )
      }
    }

    when( MasterMiniBot.MessageForwarder )
    {
      case Event( StateTimeout , stateData : MasterMiniBot.MessageForwarderStateData ) ⇒
      {
        if( getCurrentChildrenActorsCount == 0 )
          stop( FSM.Normal )
        else
          stay using stateData
      }
    }

    onTransition
    {
      case fromState -> toState ⇒ log.debug( "Change state from " + fromState + " to " + toState )
    }

    whenUnhandled
    {
      case Event( newTimeNotification : main.scala.org.kermitas.scalatron.bot.Bot.NewTimeNotification , stateData ) ⇒
      {
        //stateData match
        //{
        //  case initializedStateData : MasterMiniBot.InitializedStateData => sendToAllMyChildrenMiniBots( newTimeNotification , initializedStateData.masterMiniBotRuntimeConfig.swingDrawerOption )
        //  case _ =>
        //}

        stay using stateData
      }

      /*
      case Event( miniBotDiedNotification : MasterMiniBot.MiniBotDiedNotification , stateData ) ⇒
      {
        context.parent.tell( miniBotDiedNotification , sender )
        stay using stateData
      }*/

      case Event( MasterMiniBot.Stop( throwable ) , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received Stop message, throwable '" + throwable + "' with state data " + stateData , throwable ) ) )

      case Event( unknownMessage , stateData ) ⇒ stop( FSM.Failure( new RuntimeException( "Received unknown message '" + unknownMessage + "' (state data " + stateData + ")" ) ) )
    }

    onTermination
    {
      case StopEvent( stopType , state , stateData ) ⇒
      {
        stopType match
        {
          case FSM.Normal ⇒ log.info( "Stopping (normal), state '" + state + "', state data " + stateData )
          case FSM.Shutdown ⇒ log.info( "Stopping (shutdown), state '" + state + "', state data " + stateData )
          case FSM.Failure(cause) ⇒ log.warning( "Stopping (failure = " + cause + "), state state '" + state + "', data " + stateData )
        }

        stateData match
        {
          case initializedStateData : MasterMiniBot.InitializedStateData =>
          {
            initializedStateData.masterMiniBotRuntimeConfig.swingDrawerOption.foreach{ _ ! new SwingDrawer.Stop( None ) }
            initializedStateData.masterMiniBotRuntimeConfig.messageBroadcaster ! new MessageBroadcaster.Unregister( self )
          }

          case _ =>
        }
      }
    }

    initialize

    // ----

    protected def gotoMessageForwardSateOrStop( descriptionThrowableEither : Either[ String , Throwable ] , stateData : MasterMiniBot.InitializedStateData ) =
    {
      def gotoStop =
      {
        descriptionThrowableEither match
        {
          case scala.Right( throwable ) => stop( FSM.Failure( throwable ) )

          case scala.Left( description ) =>
          {
            log.info( description )
            stop( FSM.Normal )
          }
        }
      }

      if( stateData.masterMiniBotRuntimeConfig.amIMasterBot )
      {
        gotoStop
      }
      else
      {
        //val miniBotDiedNotification = new MasterMiniBot.MiniBotDiedNotification( stateData.masterMiniBotRuntimeConfig.scalatronName )

        //context.parent ! miniBotDiedNotification
        //stateData.masterMiniBotRuntimeConfig.gameMapKeeper ! miniBotDiedNotification
        //stateData.masterMiniBotRuntimeConfig.gameLogic ! miniBotDiedNotification

        stateData.masterMiniBotRuntimeConfig.messageBroadcaster ! new MasterMiniBot.MiniBotDiedNotification( stateData.masterMiniBotRuntimeConfig.scalatronName )

        stateData.masterMiniBotRuntimeConfig.swingDrawerOption.foreach{ swingDrawer =>
          if( stateData.masterMiniBotConfig.miniBotConfiguration.leaveDeadMiniBotViewsOnScreenToTheEndOfTheGame )
          {
            swingDrawer ! new SwingDrawer.SetStatusText( "[DEAD]" )
            swingDrawer ! new SwingDrawer.SetWindowActivity( false )
          }
          else
          {
            swingDrawer ! new SwingDrawer.Stop( None )
          }
        }

        if( getCurrentChildrenActorsCount == 0 )
        {
          gotoStop
        }
        else
        {
          val reason : String = descriptionThrowableEither match
          {
            case scala.Right( throwable ) => throwable.toString
            case scala.Left( description ) => description
          }

          log.debug( "Going into 'message forwarder' mode, reason = " + reason )

          goto( MasterMiniBot.MessageForwarder ) using new MasterMiniBot.MessageForwarderStateData( stateData.masterMiniBotConfig , stateData.masterMiniBotRuntimeConfig )
        }
      }
    }

    protected def getCurrentChildrenActorsCount : Int =
    {
      var childrenCount = 0
      context.children.foreach( _ => childrenCount += 1 )
      childrenCount
    }

    /*
    protected def sendToAllMyChildrenMiniBots( message : Any , swingDrawerOption : Option[ ActorRef ] )
    {
      swingDrawerOption match
      {
        case Some( swingDrawer ) => context.children.foreach( child => if( child != swingDrawer ) child ! message )
        case None => context.children.foreach( _ ! message )
      }
    }*/

    protected def spawnMiniBotActor( stateData : MasterMiniBot.InitializedStateData , spawnOpcode : Spawn )
    {
      log.debug( "Spawning new 'mini-bot' actor nr=" + stateData.masterMiniBotRuntimeConfig.childrenNamingCounter + ", actor path as scalatron name'" + spawnOpcode.name + "'" )

      val bornPositionOnMap = new Point( stateData.masterMiniBotRuntimeConfig.positionOnMap.x + spawnOpcode.direction.horizontalDirection.getDirectionAsInt , stateData.masterMiniBotRuntimeConfig.positionOnMap.y + spawnOpcode.direction.verticalDirection.getDirectionAsInt )

      val miniBotInitMessage = new MasterMiniBot.Init( spawnOpcode.name , false , stateData.masterMiniBotRuntimeConfig.maxStepsPerRound , stateData.masterMiniBotRuntimeConfig.maxSlavesOption , stateData.masterMiniBotConfig , stateData.masterMiniBotRuntimeConfig.swingWindowPlacer , stateData.masterMiniBotRuntimeConfig.gameMapKeeper , stateData.masterMiniBotRuntimeConfig.gameLogic , bornPositionOnMap , stateData.masterMiniBotRuntimeConfig.lastActivityTime , stateData.masterMiniBotRuntimeConfig.messageBroadcaster )

      context.actorOf( Props[ MasterMiniBot ] , "" + stateData.masterMiniBotRuntimeConfig.childrenNamingCounter ) ! miniBotInitMessage

      stateData.masterMiniBotRuntimeConfig.childrenNamingCounter += 1
    }
  }
}