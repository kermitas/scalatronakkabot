package main.scala.org.kermitas.logging
{
  import java.io._
  import java.util._
  import java.util.logging._
  import java.text._

  class SimpleFormatter extends java.util.logging.Formatter
  {
    protected val dateAndTimeFormatter = new SimpleDateFormat( "yyyy-MM-dd_HH-mm-ss" )
    protected val lineSeparator = System.getProperty( "line.separator" )

    protected val sb = new StringBuilder

    override def format( record : LogRecord ) =
    {
      sb.setLength( 0 )

      // ---

      record.getLevel().intValue() match
      {
        case 300 => sb.append( "FT" )

        case 400 => sb.append( "FR" )

        case 500 => sb.append( "F " )

        case 700 => sb.append( "C " )

        case 800 => sb.append( "I " )

        case 900 => sb.append( "W " )

        case 1000 => sb.append( "S " )

        case _ => sb.append( "??" )
      }

      // ---

      sb.append( " " )

      // ---

      val calendar = Calendar.getInstance
      calendar.setTimeInMillis( record.getMillis )

      sb.append(dateAndTimeFormatter.format(calendar.getTime))

      // ---

      sb.append( " " )

      // ---

      sb.append( record.getSourceClassName() ).append( "." ).append( record.getSourceMethodName ).append( ":" )

      // ---

      sb.append( " " )

      // ---

      sb.append( record.getMessage )

      // ---

      val throwable = record.getThrown

      if( throwable != null )
      {
        sb.append( lineSeparator )

        val baos = new ByteArrayOutputStream
        val pw = new PrintWriter( baos )
        throwable.printStackTrace( pw )
        pw.close

        sb.append( baos )
      }

      // ---

      sb.append( lineSeparator )

      // ---

      sb.toString
    }
  }
}