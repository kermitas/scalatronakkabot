import AssemblyKeys._ /* sbt-assembly */

name := "ScalatronAkkaBot"

version := "0.1"

organization := "kermitas"

scalaVersion := "2.10.1"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

//resolvers += "Local ivy2 Repository" at "file:///home/as/.ivy2/local/"

// for debugging sbt problems
logLevel := Level.Debug

// collect all dependencies
retrieveManaged := true

// --- akka
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.2-M2"

libraryDependencies += "com.typesafe.akka" %% "akka-remote" % "2.2-M2"

// --- scalatron akka commons
libraryDependencies += "kermitas" %% "scalatronakkacommons" % "0.1"

// --- scalatron akka-server akka-bot kermitas commons
libraryDependencies += "kermitas" %% "scalatronakkaserverakkabotkermitascommons" % "0.1"

// --- one jar
//seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

//libraryDependencies += "commons-lang" % "commons-lang" % "2.6"

// --- sby-assembly
assemblySettings
