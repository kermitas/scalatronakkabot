
Welcome in Scalatron Akka Bot.

// --------------------------------

All configuration (for ScalatronAkkaBot and Akka actors) is located inside application.conf (please see /src/.../resources folder).

// --------------------------------

To use external settings file specify -Dconfig.file=/full/path/to/externalConfig.conf system property.

Example (please see /dist/ folder):

java -Dconfig.file='/run/media/as/DATA/ScalatronAkkaBot/dist/externalConfig.conf' -jar ScalatronAkkaBot.jar 

// --------------------------------

Example of externalConfig.conf file (which include existing configuration and then do overrides):

include "application"

ScalatronAkkaBot
{
    logLevel = ALL
}

akka
{
    loglevel = DEBUG
}

// --------------------------------

Remember that /dist folder can be outdated:
- latest application.conf sits always inside JAR, you can unpack it and then use as external configuration file (remember to add "include "application"" as first line!!).
- also JAR file in /dist folder can be old, latest is always in /target/scala-2.10

// --------------------------------
